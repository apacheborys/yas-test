<?php
namespace Mapper;

use Entity\CountryEntity;
use Entity\CurrencyEntity;
use Entity\ValidateItemEntity;
use Validator\EntityValidator;

/**
 * Class EntityMapper
 * Mapper from raw array to entity. Please take attention - raw array must be validate before for ensuring exceptions.
 *
 * @package Mapper
 */
class EntityMapper extends AbstractMapper
{
    /**
     * Main entry method what receive raw array and returns array of entities data
     *
     * @param array $rawArray
     * @return array
     * @throws \ReflectionException
     */
    public function map(array $rawArray)
    {
        $entityValidator = new EntityValidator();
        $validationMap = $entityValidator->makeValidationMap(CountryEntity::class);

        $result = [];
        foreach ($rawArray as $keyItem => $rawItem) {
            $result[$keyItem] = $this->mapCore($rawItem, $validationMap, CountryEntity::class);
        }

        return $result;
    }

    /**
     * @param array $rawArray
     * @param array $validationMap
     * @param string $entityType
     * @return mixed
     */
    private function mapCore(array $rawArray, array $validationMap, string $entityType)
    {
        $entity = new $entityType();

        foreach ($rawArray as $keyNode => $node) {

            if ($validationMap[$keyNode]->isArray()) {
                if (!$validationMap[$keyNode]->isStandart()) {

                    $tempArray = [];
                    foreach ($rawArray[$keyNode] as $keyItem => $rawItem) {
                        $tempArray[$keyItem] = $this->mapCore(
                            $rawItem,
                            $validationMap[$keyNode]->getChild(),
                            'Entity\\' . $validationMap[$keyNode]->getVarType()
                        );
                    }
                    $entity->{'set' . ucfirst($keyNode)}($tempArray);

                } else {

                    $tempArray = [];
                    foreach ($rawArray[$keyNode] as $keyItem => $rawItem) {
                        $tempArray[$keyItem] = $this->setType($rawItem, $validationMap[$keyNode]->getVarType());
                    }
                    $entity->{'set' . ucfirst($keyNode)}($tempArray);

                }
            } else {
                $entity->{'set' . ucfirst($keyNode)}($this->setType($node, $validationMap[$keyNode]->getVarType()));
            }
        }

        return $entity;
    }

    /**
     * @param $data
     * @param $type
     * @return bool|string|int|float|double
     */
    private function setType($data, $type)
    {
        if (!in_array($type, ValidateItemEntity::STANDART_VAR_TYPES)) {
            return false;
        }

        eval('$result = (' . $type . ')$data;');

        return $result;
    }
}