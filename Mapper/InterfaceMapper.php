<?php
namespace Mapper;

/**
 * Interface InterfaceMapper
 * @package Mapper
 */
interface InterfaceMapper
{
    public function map(array $rawArray);
}