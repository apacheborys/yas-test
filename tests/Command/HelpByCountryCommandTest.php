<?php
namespace Tests\Command;

use Command\HelpByCountryCommand;
use Entity\CountryEntity;
use Entity\LanguageEntity;
use PHPUnit\Framework\TestCase;
use Tests\Command\Mock\CompareCountriesCommandMock;
use Tests\Command\Mock\HelpByCountryCommandMock;

class HelpByCountryCommandTest extends TestCase
{
    /**
     * Module testing
     *
     * @param $params
     * @param $mock
     * @param $expected
     * @covers \Command\HelpByCountryCommand::run()
     * @dataProvider runProvider
     */
    public function testRun($params, $mock, $expected)
    {
        $command = new HelpByCountryCommandMock();
        $command->mockValue = $mock;

        $this->assertEquals($expected, $command->run($params));
    }

    /**
     * Functionally testing
     *
     * @param $params
     * @param $expected
     * @covers \Command\HelpByCountryCommand::run()
     * @dataProvider realRunProvider
     */
    public function testRealRun($params, $expected)
    {
        $command = new HelpByCountryCommand();

        $this->assertEquals($expected, $command->run($params));
    }

    /**
     * @see testRun
     * @case 1 several countries with same languages
     * @return array
     */
    public function runProvider()
    {
        $originCountry = new CountryEntity();
        $originCountry->setName('Spain');
        $language = new LanguageEntity();
        $language->setIso639_1('es');
        $originCountry->setLanguages([$language]);

        $findCountries = [];
        $findCountry = new CountryEntity();
        $findCountry->setName('Chile');
        $findCountries[] = $findCountry;

        $findCountry = new CountryEntity();
        $findCountry->setName('Colombia');
        $findCountries[] = $findCountry;

        return [
            [
                ['index.php', 'Spain'],
                ['origin' => $originCountry, 'find' => $findCountries],
                'Country language code: es' . PHP_EOL . 'Spain speaks same language with these countries: Chile, Colombia ...' . PHP_EOL,
            ],
        ];
    }

    /**
     * @case 0 similar language
     * @case 1 wrong country
     * @see testRealRunProvider
     * @return array
     */
    public function realRunProvider()
    {
        return [
            [
                ['index.php', 'Spain'],
                'Country language code: es' . PHP_EOL .
                'Spain speaks same language with these countries:' .
                ' Argentina, Belize, Bolivia (Plurinational State of), Chile, Colombia, Costa Rica, Cuba, Dominican Republic, Ecuador, ' .
                'El Salvador, Equatorial Guinea, Guam, Guatemala, Honduras, Mexico, Nicaragua, Panama, Paraguay, Peru, Puerto Rico, ' .
                'Spain, Uruguay, Venezuela (Bolivarian Republic of), Western Sahara ...' . PHP_EOL,
            ],
            [
                ['index.php', 'fjsdl'],
                'Can\'t find country fjsdl' . PHP_EOL,
            ],
        ];
    }
}
