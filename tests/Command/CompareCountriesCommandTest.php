<?php
namespace Tests\Command;

use Command\CompareCountriesCommand;
use PHPUnit\Framework\TestCase;
use Tests\Command\Mock\CompareCountriesCommandMock;

class CompareCountriesCommandTest extends TestCase
{
    /**
     * Module testing
     *
     * @param $params
     * @param $mock
     * @param $expected
     * @covers \Command\CompareCountriesCommand::run()
     * @dataProvider runProvider
     */
    public function testRun($params, $mock, $expected)
    {
        $command = new CompareCountriesCommandMock();
        $command->mockValue = $mock;

        $this->assertEquals($expected, $command->run($params));
    }

    /**
     * Functionally testing
     *
     * @param $params
     * @param $expected
     * @covers \Command\CompareCountriesCommand::run()
     * @dataProvider realRunProvider
     */
    public function testRealRun($params, $expected)
    {
        $command = new CompareCountriesCommand();

        $this->assertEquals($expected, $command->run($params));
    }

    /**
     * @see testRun
     * @case 1 similar language
     * @case 2 different languages
     * @return array
     */
    public function runProvider()
    {
        return [
            [
                ['index.php', 'Spain', 'France'],
                true,
                'Spain and France are speak the same language' . PHP_EOL
            ],
            [
                ['index.php', 'Spain', 'France'],
                false,
                'Spain and France do not speak the same language' . PHP_EOL
            ],
        ];
    }

    /**
     * @case 0 similar language
     * @case 1 different languages
     * @case 2 wrong country
     * @case 3 send excess
     * @see testRealRunProvider
     * @return array
     */
    public function realRunProvider()
    {
        return [
            [
                ['index.php', 'Spain', 'Chile'],
                'Spain and Chile are speak the same language' . PHP_EOL
            ],
            [
                ['index.php', 'Spain', 'France'],
                'Spain and France do not speak the same language' . PHP_EOL
            ],
            [
                ['index.php', 'Spain', 'fjsdl'],
                'Can\'t find country fjsdl' . PHP_EOL,
            ],
            [
                ['index.php', 'Spain', 'France', 'eijfa;djf;a'],
                'Spain and France do not speak the same language' . PHP_EOL
            ],
        ];
    }
}
