<?php
namespace Tests\Command\Mock;

use Command\HelpByCountryCommand;

class HelpByCountryCommandMock extends HelpByCountryCommand
{
    public $mockValue;

    public function callToController(string $class, array $array)
    {
        return $this->mockValue;
    }
}