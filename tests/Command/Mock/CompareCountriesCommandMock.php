<?php
namespace Tests\Command\Mock;

use Command\CompareCountriesCommand;

class CompareCountriesCommandMock extends CompareCountriesCommand
{
    public $mockValue;

    public function callToController(string $class, array $array)
    {
        return $this->mockValue;
    }
}