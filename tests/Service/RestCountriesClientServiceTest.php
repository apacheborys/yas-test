<?php
namespace Service;

use Entity\CountryEntity;
use Entity\CurrencyEntity;
use Entity\LanguageEntity;
use Entity\RegionEntity;
use PHPUnit\Framework\TestCase;
use Tests\Service\Mock\RestCountriesClientServiceMock;

class RestCountriesClientServiceTest extends TestCase
{
    /**
     * @covers \Service\RestCountriesClientService::getAllCountries()
     * @dataProvider getAllCountriesProvider
     */
    public function testGetAllCountries($params, $expected)
    {
        $restClient = new RestCountriesClientServiceMock();
        $restClient->mockValue = $params;

        $response = $restClient->getAllCountries();

        $this->assertEquals($expected, $response);
    }

    /**
     * @see testGetAllCountries
     * @return array
     */
    public function getAllCountriesProvider()
    {
        $country = new CountryEntity();
        $country->setName('Afghanistan');
        $country->setTopLevelDomain([".af"]);
        $country->setAlpha2Code('AF');
        $country->setAlpha3Code('AFG');
        $country->setCallingCodes(["93"]);
        $country->setCapital('Kabul');
        $country->setAltSpellings(["AF","Afġānistān"]);
        $country->setRegion('Asia');
        $country->setSubregion('Southern Asia');
        $country->setPopulation(27657145);
        $country->setLatlng([33.0,65.0]);
        $country->setDemonym('Afghan');
        $country->setArea(652230.0);
        $country->setGini(27.8);
        $country->setTimezones(["UTC+04:30"]);
        $country->setBorders(["IRN","PAK","TKM","UZB","TJK","CHN"]);
        $country->setNativeName("افغانستان");
        $country->setNumericCode('004');

        $currency = new CurrencyEntity();
        $currency->setName('Afghan afghani');
        $currency->setCode('AFN');
        $currency->setSymbol("؋");

        $country->setCurrencies([$currency]);

        $languagePs = new LanguageEntity();
        $languagePs->setName('Pashto');
        $languagePs->setIso639_1('ps');
        $languagePs->setIso639_2('pus');
        $languagePs->setNativeName("پښتو");

        $languageUz = new LanguageEntity();
        $languageUz->setName('Uzbek');
        $languageUz->setIso639_1('uz');
        $languageUz->setIso639_2('uzb');
        $languageUz->setNativeName("Oʻzbek");

        $languageTk = new LanguageEntity();
        $languageTk->setName('Turkmen');
        $languageTk->setIso639_1('tk');
        $languageTk->setIso639_2('tuk');
        $languageTk->setNativeName("Türkmen");

        $country->setLanguages([$languagePs, $languageUz, $languageTk]);
        $country->setTranslations([
              "de" => "Afghanistan",
              "es" => "Afganistán",
              "fr" => "Afghanistan",
              "ja" => "アフガニスタン",
              "it" => "Afghanistan",
              "br" => "Afeganistão",
              "pt" => "Afeganistão",
              "nl" => "Afghanistan",
              "hr" => "Afganistan",
              "fa" => "افغانستان",
            ]
        );
        $country->setFlag('https://restcountries.eu/data/afg.svg');

        $regionalBloc = new RegionEntity();
        $regionalBloc->setName('South Asian Association for Regional Cooperation');
        $regionalBloc->setAcronym('SAARC');
        $regionalBloc->setOtherAcronyms([]);
        $regionalBloc->setOtherNames([]);

        $country->setRegionalBlocs([$regionalBloc]);
        $country->setCioc('AFG');

        return [
            [
                '[
  {
    "name": "Afghanistan",
    "topLevelDomain": [
      ".af"
    ],
    "alpha2Code": "AF",
    "alpha3Code": "AFG",
    "callingCodes": [
      "93"
    ],
    "capital": "Kabul",
    "altSpellings": [
      "AF",
      "Afġānistān"
    ],
    "region": "Asia",
    "subregion": "Southern Asia",
    "population": 27657145,
    "latlng": [
      33.0,
      65.0
    ],
    "demonym": "Afghan",
    "area": 652230.0,
    "gini": 27.8,
    "timezones": [
      "UTC+04:30"
    ],
    "borders": [
      "IRN",
      "PAK",
      "TKM",
      "UZB",
      "TJK",
      "CHN"
    ],
    "nativeName": "افغانستان",
    "numericCode": "004",
    "currencies": [
      {
        "code": "AFN",
        "name": "Afghan afghani",
        "symbol": "؋"
      }
    ],
    "languages": [
      {
        "iso639_1": "ps",
        "iso639_2": "pus",
        "name": "Pashto",
        "nativeName": "پښتو"
      },
      {
        "iso639_1": "uz",
        "iso639_2": "uzb",
        "name": "Uzbek",
        "nativeName": "Oʻzbek"
      },
      {
        "iso639_1": "tk",
        "iso639_2": "tuk",
        "name": "Turkmen",
        "nativeName": "Türkmen"
      }
    ],
    "translations": {
      "de": "Afghanistan",
      "es": "Afganistán",
      "fr": "Afghanistan",
      "ja": "アフガニスタン",
      "it": "Afghanistan",
      "br": "Afeganistão",
      "pt": "Afeganistão",
      "nl": "Afghanistan",
      "hr": "Afganistan",
      "fa": "افغانستان"
    },
    "flag": "https://restcountries.eu/data/afg.svg",
    "regionalBlocs": [
      {
        "acronym": "SAARC",
        "name": "South Asian Association for Regional Cooperation",
        "otherAcronyms": [],
        "otherNames": []
      }
    ],
    "cioc": "AFG"
  }]',
                [$country],
            ]
        ];
    }
}