<?php
namespace Tests\Service;

use PHPUnit\Framework\TestCase;
use Service\JsonDataService;

class JsonDataServiceTest extends TestCase
{
    /**
     * @covers \Service\JsonDataService
     * @param $param
     * @param $expected
     * @param $lastError
     * @dataProvider serviceProvider
     */
    public function testService($param, $expected, $lastError)
    {
        $service = new JsonDataService($param);

        $this->assertEquals($expected, $service->resultArray);
        $this->assertEquals($lastError, $service->lastError);
    }

    /**
     * @see testService
     * @return array
     */
    public function serviceProvider()
    {
        return [
            [
                '{"menu": {  "id": "file",  "value": "File",  "popup": {    "menuitem": [      {"value": "New", "onclick": "CreateNewDoc()"},      {"value": "Open", "onclick": "OpenDoc()"},      {"value": "Close", "onclick": "CloseDoc()"}    ]  }}}',
                [
                    'menu' => [
                        'id' => 'file',
                        'value' => 'File',
                        'popup' => [
                            'menuitem' => [
                                [
                                    'value' => 'New',
                                    'onclick' => 'CreateNewDoc()',
                                ],
                                [
                                    'value' => 'Open',
                                    'onclick' => 'OpenDoc()',
                                ],
                                [
                                    'value' => 'Close',
                                    'onclick' => 'CloseDoc()',
                                ],
                            ],
                        ],
                    ],
                ],
                null,
            ],
            [
                'jfa;jdfa',
                false,
                'Syntax error, malformed JSON',
            ]
        ];
    }
}