<?php
namespace Tests\Service\Mock;

use Service\RestCountriesClientService;

class RestCountriesClientServiceMock extends RestCountriesClientService
{
    public $mockValue;

    public function __construct()
    {
        $this->httpClient = new HttpClientServiceMock();
        $this->httpClient->mockValue = 200;
    }

    public function makeRequest(string $uri): string
    {
        return $this->mockValue;
    }
}