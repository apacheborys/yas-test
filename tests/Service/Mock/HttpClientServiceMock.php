<?php
namespace Tests\Service\Mock;

use Service\HttpClientService;

class HttpClientServiceMock extends HttpClientService
{
    public $mockValue;

    public function getResponseCode(): int
    {
        return $this->mockValue;
    }
}