<?php
namespace Tests\Repository;

use Entity\CountryEntity;
use Entity\CurrencyEntity;
use Entity\LanguageEntity;
use Entity\RegionEntity;
use PHPUnit\Framework\TestCase;
use Repository\CountryRepository;

class CountryRepositoryTest extends TestCase
{
    /**
     * @var CountryRepository $repository
     */
    public $repository;

    /**
     * @var CountryEntity $countryEntity
     */
    public $countryEntity;

    /**
     * @before
     */
    public function init()
    {
        $this->repository = new CountryRepository();
    }

    public function dataInit()
    {
        $this->countryEntity = new CountryEntity();
        $this->countryEntity->setName('Afghanistan');
        $this->countryEntity->setTopLevelDomain([".af"]);
        $this->countryEntity->setAlpha2Code('AF');
        $this->countryEntity->setAlpha3Code('AFG');
        $this->countryEntity->setCallingCodes(["93"]);
        $this->countryEntity->setCapital('Kabul');
        $this->countryEntity->setAltSpellings(["AF","Afġānistān"]);
        $this->countryEntity->setRegion('Asia');
        $this->countryEntity->setSubregion('Southern Asia');
        $this->countryEntity->setPopulation(27657145);
        $this->countryEntity->setLatlng([33.0,65.0]);
        $this->countryEntity->setDemonym('Afghan');
        $this->countryEntity->setArea(652230.0);
        $this->countryEntity->setGini(27.8);
        $this->countryEntity->setTimezones(["UTC+04:30"]);
        $this->countryEntity->setBorders(["IRN","PAK","TKM","UZB","TJK","CHN"]);
        $this->countryEntity->setNativeName("افغانستان");
        $this->countryEntity->setNumericCode('004');

        $currency = new CurrencyEntity();
        $currency->setName('Afghan afghani');
        $currency->setCode('AFN');
        $currency->setSymbol("؋");

        $this->countryEntity->setCurrencies([$currency]);

        $languagePs = new LanguageEntity();
        $languagePs->setName('Pashto');
        $languagePs->setIso639_1('ps');
        $languagePs->setIso639_2('pus');
        $languagePs->setNativeName("پښتو");

        $languageUz = new LanguageEntity();
        $languageUz->setName('Uzbek');
        $languageUz->setIso639_1('uz');
        $languageUz->setIso639_2('uzb');
        $languageUz->setNativeName("Oʻzbek");

        $languageTk = new LanguageEntity();
        $languageTk->setName('Turkmen');
        $languageTk->setIso639_1('tk');
        $languageTk->setIso639_2('tuk');
        $languageTk->setNativeName("Türkmen");

        $this->countryEntity->setLanguages([$languagePs, $languageUz, $languageTk]);
        $this->countryEntity->setTranslations([
                "de" => "Afghanistan",
                "es" => "Afganistán",
                "fr" => "Afghanistan",
                "ja" => "アフガニスタン",
                "it" => "Afghanistan",
                "br" => "Afeganistão",
                "pt" => "Afeganistão",
                "nl" => "Afghanistan",
                "hr" => "Afganistan",
                "fa" => "افغانستان",
            ]
        );
        $this->countryEntity->setFlag('https://restcountries.eu/data/afg.svg');

        $regionalBloc = new RegionEntity();
        $regionalBloc->setName('South Asian Association for Regional Cooperation');
        $regionalBloc->setAcronym('SAARC');
        $regionalBloc->setOtherAcronyms([]);
        $regionalBloc->setOtherNames([]);

        $this->countryEntity->setRegionalBlocs([$regionalBloc]);
        $this->countryEntity->setCioc('AFG');
    }

    /**
     * @param $data
     * @param $params
     * @param $expected
     * @covers \Repository\CountryRepository::findBy()
     * @dataProvider findByProvider
     */
    public function testFindBy($data, $params, $expected)
    {
        $this->repository->loadData($data);

        $this->assertEquals($expected, $this->repository->findBy($params));
    }

    /**
     * @param $data
     * @param $params
     * @param $expected
     * @covers \Repository\CountryRepository::findByOne()
     * @dataProvider findByOneProvider
     */
    public function testFindByOne($data, $params, $expected)
    {
        $this->repository->loadData($data);

        $this->assertEquals($expected, $this->repository->findByOne($params));
    }

    /**
     * @dataProvider findSimilarLanguageProvider
     * @covers \Repository\CountryRepository::findSimilarLanguage()
     * @param $data
     * @param $params
     * @param $expected
     */
    public function testFindSimilarLanguage($data, $params, $expected)
    {
        $this->repository->loadData($data);

        $this->assertEquals($expected, $this->repository->findSimilarLanguage($params));
    }

    /**
     * @see testFindBy
     * @return array
     */
    public function findByProvider()
    {
        if (is_null($this->countryEntity)) {
            $this->dataInit();
        }

        return [
            [
                [$this->countryEntity],
                ['name' => 'Afghanistan'],
                [$this->countryEntity],
            ],
            [
                [$this->countryEntity],
                ['name' => 'Spain'],
                []
            ],
        ];
    }

    /**
     * @see testFindByOne
     * @return array
     */
    public function findByOneProvider()
    {
        if (is_null($this->countryEntity)) {
            $this->dataInit();
        }

        return [
            [
                [$this->countryEntity, $this->countryEntity],
                ['name' => 'Afghanistan'],
                $this->countryEntity,
            ],
            [
                [$this->countryEntity],
                ['name' => 'Spain'],
                false,
            ],
        ];
    }

    /**
     * @see testFindSimilarLanguage
     * @return array
     */
    public function findSimilarLanguageProvider()
    {
        if (is_null($this->countryEntity)) {
            $this->dataInit();
        }

        return [
            [
                [$this->countryEntity],
                ['ps'],
                [$this->countryEntity],
            ],
            [
                [$this->countryEntity],
                ['sp', 'en'],
                [],
            ],
        ];
    }
}