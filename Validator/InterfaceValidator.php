<?php
namespace Validator;

/**
 * Interface InterfaceValidator
 * @package Validator
 */
interface InterfaceValidator
{
    /**
     * @param $rawData
     * @return bool
     */
    public function validate($rawData):bool;

    /**
     * @param string $entity
     * @return array
     */
    public function makeValidationMap(string $entity):array;
}