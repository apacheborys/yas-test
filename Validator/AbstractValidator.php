<?php
namespace Validator;

use Entity\ValidateItemEntity;

/**
 * Class AbstractValidator
 * @package Validator
 */
abstract class AbstractValidator
{
    /**
     * @param string $entity
     * @return ValidateItemEntity[]
     * @throws \ReflectionException
     */
    public function makeValidationMap(string $entity):array
    {
        $reflector = new \ReflectionClass($entity);

        $prefix = substr($entity, 0, strrpos($entity, '\\'));

        $arrayForValidate = [];
        foreach (get_class_methods($entity) as $method) {
            if (substr($method, 0, 3) !== 'set') {
                continue;
            }

            $docBlock = $reflector->getMethod($method)->getDocComment();

            $paramEntry = strpos($docBlock, '@param');
            $paramEnd = strpos($docBlock, PHP_EOL, $paramEntry + 1);

            $param = substr($docBlock, $paramEntry, $paramEnd - $paramEntry);
            $param = explode(' ', $param);

            if (!isset($param[1])) {
                continue;
            }

            $typeVar = trim($param[1]);
            $varName = lcfirst(substr($method, 3));

            if (substr($typeVar, -2) === '[]') {
                $isArray = true;
                $typeVar = substr($typeVar, 0, -2);
            } else {
                $isArray = false;
            }

            if (in_array($typeVar, ValidateItemEntity::STANDART_VAR_TYPES)) {
                $isStandart = true;
            } else {
                $isStandart = false;
            }

            $validateItem = new ValidateItemEntity();
            $validateItem->setVarName($varName);
            $validateItem->setVarType($typeVar);
            $validateItem->setIsArray($isArray);
            $validateItem->setIsStandart($isStandart);
            $validateItem->setChild($isStandart ? null : $this->makeValidationMap($prefix . '\\' . $typeVar));

            $arrayForValidate[$varName] = $validateItem;
        }

        return $arrayForValidate;
    }
}