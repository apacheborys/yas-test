<?php
namespace Validator;

use Entity\CountryEntity;
use Entity\ValidateItemEntity;

/**
 * Class EntityValidator
 * Validation of entity. Now it made hardly for CountryEntity, but you can improve it - add one argument where define
 * entity class. Please don't forgot about improve mapper...
 *
 * @package Validator
 */
class EntityValidator extends AbstractValidator
{
    /**
     * Main function
     *
     * @param array $rawArray
     * @return bool
     * @throws \ReflectionException
     */
    public function validate(array $rawArray)
    {
        $validationMap = $this->makeValidationMap(CountryEntity::class);

        foreach ($rawArray as $rawItem) {
            $result = $this->validationCore($rawItem, $validationMap);
            if (is_string($result)) {
                return $result;
            }
        }

        return true;
    }

    /**
     * @param array $rawArray
     * @param ValidateItemEntity[] $validationMap
     * @return bool|string
     */
    private function validationCore(array $rawArray, array $validationMap)
    {
        foreach ($rawArray as $keyNode => $node) {
            if (!isset($validationMap[$keyNode])) {
                return'Unexpected key ' . $keyNode . ' during validation ' . CountryEntity::class . ' ' . serialize($rawArray) . ' ' . serialize($validationMap);
            }

            if ($validationMap[$keyNode]->isArray() && !$validationMap[$keyNode]->isStandart()) {
                foreach ($rawArray[$keyNode] as $rawItem) {
                    $this->validationCore($rawItem, $validationMap[$keyNode]->getChild());
                }
            }
        }

        $difference = array_diff_key($rawArray, $validationMap);

        if ($difference) {
            return 'Difference between array keys: ' . serialize($difference);
        }

        return true;
    }
}