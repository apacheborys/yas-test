<?php

function libraryRoot($classname) {
    $filename = str_replace('\\',DIRECTORY_SEPARATOR, $classname) .".php";
    require_once($filename);
}

spl_autoload_register('libraryRoot');