<?php
namespace Entity;

/**
 * Class CountryEntity
 * Main entity for country
 *
 * @package Entity
 */
class CountryEntity
{
    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string[] $topLevelDomain
     */
    private $topLevelDomain;

    /**
     * @var string $alpha2Code
     */
    private $alpha2Code;

    /**
     * @var string $alpha3Code
     */
    private $alpha3Code;

    /**
     * @var int[] $callingCodes
     */
    private $callingCodes;

    /**
     * @var string $capital
     */
    private $capital;

    /**
     * @var string[] $altSpellings
     */
    private $altSpellings;

    /**
     * @var string $region
     */
    private $region;

    /**
     * @var string $subregion
     */
    private $subregion;

    /**
     * @var int $population
     */
    private $population;

    /**
     * @var float[] $latlng
     */
    private $latlng;

    /**
     * @var string $demonym
     */
    private $demonym;

    /**
     * @var float $area
     */
    private $area;

    /**
     * @var float $gini
     */
    private $gini;

    /**
     * @var string[] $timezones
     */
    private $timezones;

    /**
     * @var string[] $borders
     */
    private $borders;

    /**
     * @var string $nativeName
     */
    private $nativeName;

    /**
     * @var string $numericCode
     */
    private $numericCode;

    /**
     * @var CurrencyEntity[] $currencies
     */
    private $currencies;

    /**
     * @var LanguageEntity[] $languages
     */
    private $languages;

    /**
     * @var string[] $translations
     */
    private $translations;

    /**
     * @var string $flag
     */
    private $flag;

    /**
     * @var RegionEntity[] $regionalBlocs
     */
    private $regionalBlocs;

    /**
     * @var string $cioc
     */
    private $cioc;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string[]
     */
    public function getTopLevelDomain(): array
    {
        return $this->topLevelDomain;
    }

    /**
     * @param string[] $topLevelDomain
     */
    public function setTopLevelDomain(array $topLevelDomain): void
    {
        $this->topLevelDomain = $topLevelDomain;
    }

    /**
     * @return string
     */
    public function getAlpha2Code(): string
    {
        return $this->alpha2Code;
    }

    /**
     * @param string $alpha2Code
     */
    public function setAlpha2Code(string $alpha2Code): void
    {
        $this->alpha2Code = $alpha2Code;
    }

    /**
     * @return string
     */
    public function getAlpha3Code(): string
    {
        return $this->alpha3Code;
    }

    /**
     * @param string $alpha3Code
     */
    public function setAlpha3Code(string $alpha3Code): void
    {
        $this->alpha3Code = $alpha3Code;
    }

    /**
     * @return int[]
     */
    public function getCallingCodes(): array
    {
        return $this->callingCodes;
    }

    /**
     * @param int[] $callingCodes
     */
    public function setCallingCodes(array $callingCodes): void
    {
        $this->callingCodes = $callingCodes;
    }

    /**
     * @return string
     */
    public function getCapital(): string
    {
        return $this->capital;
    }

    /**
     * @param string $capital
     */
    public function setCapital(string $capital): void
    {
        $this->capital = $capital;
    }

    /**
     * @return string[]
     */
    public function getAltSpellings(): array
    {
        return $this->altSpellings;
    }

    /**
     * @param string[] $altSpellings
     */
    public function setAltSpellings(array $altSpellings): void
    {
        $this->altSpellings = $altSpellings;
    }

    /**
     * @return string
     */
    public function getRegion(): string
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion(string $region): void
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getSubregion(): string
    {
        return $this->subregion;
    }

    /**
     * @param string $subregion
     */
    public function setSubregion(string $subregion): void
    {
        $this->subregion = $subregion;
    }

    /**
     * @return int
     */
    public function getPopulation(): int
    {
        return $this->population;
    }

    /**
     * @param int $population
     */
    public function setPopulation(int $population): void
    {
        $this->population = $population;
    }

    /**
     * @return float[]
     */
    public function getLatlng(): array
    {
        return $this->latlng;
    }

    /**
     * @param float[] $latlng
     */
    public function setLatlng(array $latlng): void
    {
        $this->latlng = $latlng;
    }

    /**
     * @return string
     */
    public function getDemonym(): string
    {
        return $this->demonym;
    }

    /**
     * @param string $demonym
     */
    public function setDemonym(string $demonym): void
    {
        $this->demonym = $demonym;
    }

    /**
     * @return float
     */
    public function getArea(): float
    {
        return $this->area;
    }

    /**
     * @param float $area
     */
    public function setArea(float $area): void
    {
        $this->area = $area;
    }

    /**
     * @return float
     */
    public function getGini(): float
    {
        return $this->gini;
    }

    /**
     * @param float $gini
     */
    public function setGini(float $gini): void
    {
        $this->gini = $gini;
    }

    /**
     * @return string[]
     */
    public function getTimezones(): array
    {
        return $this->timezones;
    }

    /**
     * @param string[] $timezones
     */
    public function setTimezones(array $timezones): void
    {
        $this->timezones = $timezones;
    }

    /**
     * @return string[]
     */
    public function getBorders(): array
    {
        return $this->borders;
    }

    /**
     * @param string[] $borders
     */
    public function setBorders(array $borders): void
    {
        $this->borders = $borders;
    }

    /**
     * @return string
     */
    public function getNativeName(): string
    {
        return $this->nativeName;
    }

    /**
     * @param string $nativeName
     */
    public function setNativeName(string $nativeName): void
    {
        $this->nativeName = $nativeName;
    }

    /**
     * @return string
     */
    public function getNumericCode(): string
    {
        return $this->numericCode;
    }

    /**
     * @param string $numericCode
     */
    public function setNumericCode(string $numericCode): void
    {
        $this->numericCode = $numericCode;
    }

    /**
     * @return CurrencyEntity[]
     */
    public function getCurrencies(): array
    {
        return $this->currencies;
    }

    /**
     * @param CurrencyEntity[] $currencies
     */
    public function setCurrencies(array $currencies): void
    {
        $this->currencies = $currencies;
    }

    /**
     * @return LanguageEntity[]
     */
    public function getLanguages(): array
    {
        return $this->languages;
    }

    /**
     * @param string $key
     * @return array
     */
    public function getLanguagesAsArray(string $key): array
    {
        $languagesForFind = [];
        foreach ($this->getLanguages() as $language) {
            $languagesForFind[] = $language->{'get' . $key}();
        }

        return $languagesForFind;
    }

    /**
     * @param LanguageEntity[] $languages
     */
    public function setLanguages(array $languages): void
    {
        $this->languages = $languages;
    }

    /**
     * @return string[]
     */
    public function getTranslations(): array
    {
        return $this->translations;
    }

    /**
     * @param string[] $translations
     */
    public function setTranslations(array $translations): void
    {
        $this->translations = $translations;
    }

    /**
     * @return string
     */
    public function getFlag(): string
    {
        return $this->flag;
    }

    /**
     * @param string $flag
     */
    public function setFlag(string $flag): void
    {
        $this->flag = $flag;
    }

    /**
     * @return RegionEntity[]
     */
    public function getRegionalBlocs(): array
    {
        return $this->regionalBlocs;
    }

    /**
     * @param RegionEntity[] $regionalBlocs
     */
    public function setRegionalBlocs(array $regionalBlocs): void
    {
        $this->regionalBlocs = $regionalBlocs;
    }

    /**
     * @return string
     */
    public function getCioc(): string
    {
        return $this->cioc;
    }

    /**
     * @param string $cioc
     */
    public function setCioc(string $cioc): void
    {
        $this->cioc = $cioc;
    }
}