<?php
namespace Entity;

/**
 * Class ValidateItemEntity
 * Entity for building validation map. Each element in any entity must be described of this entity.
 *
 * @package Entity
 */
class ValidateItemEntity
{
    public const STANDART_VAR_TYPES = [
        'int', 'integer', 'float', 'double', 'string', 'bool', 'boolean'
    ];

    /**
     * @var string $varName
     */
    private $varName;

    /**
     * @var string $varType
     */
    private $varType;

    /**
     * @var bool $isArray
     */
    private $isArray;

    /**
     * @var bool $isStandart
     */
    private $isStandart;

    /**
     * @var null|ValidateItemEntity[] $child
     */
    private $child;

    /**
     * @return string
     */
    public function getVarName(): string
    {
        return $this->varName;
    }

    /**
     * @param string $varName
     */
    public function setVarName(string $varName): void
    {
        $this->varName = $varName;
    }

    /**
     * @return string
     */
    public function getVarType(): string
    {
        return $this->varType;
    }

    /**
     * @param string $varType
     */
    public function setVarType(string $varType): void
    {
        $this->varType = $varType;
    }

    /**
     * @return bool
     */
    public function isArray(): bool
    {
        return $this->isArray;
    }

    /**
     * @param bool $isArray
     */
    public function setIsArray(bool $isArray): void
    {
        $this->isArray = $isArray;
    }

    /**
     * @return bool
     */
    public function isStandart(): bool
    {
        return $this->isStandart;
    }

    /**
     * @param bool $isStandart
     */
    public function setIsStandart(bool $isStandart): void
    {
        $this->isStandart = $isStandart;
    }

    /**
     * @return ValidateItemEntity[]|null
     */
    public function getChild(): ?array
    {
        return $this->child;
    }

    /**
     * @param ValidateItemEntity[]|null $child
     */
    public function setChild(?array $child): void
    {
        $this->child = $child;
    }
}