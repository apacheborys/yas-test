<?php
namespace Entity;

/**
 * Class RegionEntity
 * Region node inside Country entity
 *
 * @package Entity
 */
class RegionEntity
{
    /**
     * @var string $acronym
     */
    private $acronym;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string[] $otherAcronyms
     */
    private $otherAcronyms;

    /**
     * @var string[] $otherNames
     */
    private $otherNames;

    /**
     * @return string
     */
    public function getAcronym(): string
    {
        return $this->acronym;
    }

    /**
     * @param string $acronym
     */
    public function setAcronym(string $acronym): void
    {
        $this->acronym = $acronym;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string[]
     */
    public function getOtherAcronyms(): array
    {
        return $this->otherAcronyms;
    }

    /**
     * @param string[] $otherAcronyms
     */
    public function setOtherAcronyms(array $otherAcronyms): void
    {
        $this->otherAcronyms = $otherAcronyms;
    }

    /**
     * @return string[]
     */
    public function getOtherNames(): array
    {
        return $this->otherNames;
    }

    /**
     * @param string[] $otherNames
     */
    public function setOtherNames(array $otherNames): void
    {
        $this->otherNames = $otherNames;
    }
}