<?php
namespace Entity;

/**
 * Class LanguageEntity
 * Language node inside country entity
 *
 * @package Entity
 */
class LanguageEntity
{
    /**
     * @var string $iso639_1
     */
    private $iso639_1;

    /**
     * @var string $iso639_2
     */
    private $iso639_2;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $nativeName
     */
    private $nativeName;

    /**
     * @return string
     */
    public function getIso639_1(): string
    {
        return $this->iso639_1;
    }

    /**
     * @param string $iso639_1
     */
    public function setIso639_1(string $iso639_1): void
    {
        $this->iso639_1 = $iso639_1;
    }

    /**
     * @return string
     */
    public function getIso639_2(): string
    {
        return $this->iso639_2;
    }

    /**
     * @param string $iso639_2
     */
    public function setIso639_2(string $iso639_2): void
    {
        $this->iso639_2 = $iso639_2;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getNativeName(): string
    {
        return $this->nativeName;
    }

    /**
     * @param string $nativeName
     */
    public function setNativeName(string $nativeName): void
    {
        $this->nativeName = $nativeName;
    }
}