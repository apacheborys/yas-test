<?php
namespace Controller;

use Entity\CountryEntity;
use Repository\CountryRepository;
use Service\RestCountriesClientService;

/**
 * Class HelpByCountryController
 * Find out what countries speak in same language in which language speak country what we receive in argv[2]
 *
 * @package Controller
 */
class HelpByCountryController extends AbstractController
{
    /**
     * @param mixed ...$arguments
     * @return array|bool|mixed
     * @throws \ReflectionException
     */
    public function process(...$arguments)
    {
        $client = new RestCountriesClientService();
        $countries = $client->getAllCountries();

        $repository = new CountryRepository();
        $repository->loadData($countries);

        $originCountry = $repository->findByOne(['name' => $arguments[0][1]]);

        if (!$originCountry) {
            return ['error' => 'Can\'t find country ' . $arguments[0][1] . PHP_EOL];
        }

        $findCountries = $repository->findSimilarLanguage($originCountry->getLanguagesAsArray('iso639_1'));

        return ['origin' => $originCountry, 'find' => $findCountries];
    }
}