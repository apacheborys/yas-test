<?php
namespace Controller;

/**
 * Interface InterfaceController
 * @package Controller
 */
interface InterfaceController
{
    /**
     * Key function in controller what must implement functionality
     *
     * @param mixed ...$arguments
     * @return mixed
     */
    public function process(...$arguments);
}