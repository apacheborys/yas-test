<?php
namespace Controller;

use Entity\CountryEntity;
use Repository\CountryRepository;
use Service\RestCountriesClientService;

/**
 * Class CompareCountryLangController
 * Main logic for comparing two countries language
 *
 * @package Controller
 */
class CompareCountryLangController extends AbstractController
{
    public function process(...$arguments)
    {
        $client = new RestCountriesClientService();
        $countries = $client->getAllCountries();

        $repository = new CountryRepository();
        $repository->loadData($countries);

        $firstCountry = $repository->findByOne(['name' => $arguments[0][1]]);

        if (!$firstCountry) {
            return ['error' => 'Can\'t find country ' . $arguments[0][1] . PHP_EOL];
        }

        $secondCountry = $repository->findByOne(['name' => $arguments[0][2]]);

        if (!$secondCountry) {
            return ['error' => 'Can\'t find country ' . $arguments[0][2] . PHP_EOL];
        }

        $result = array_intersect(
            $firstCountry->getLanguagesAsArray('iso639_1'),
            $secondCountry->getLanguagesAsArray('iso639_1')
        );

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}