<?php
namespace Command;

use Service\RestCountriesClientService;

/**
 * Class AbstractCommand
 * @package Command
 */
abstract class AbstractCommand implements InterfaceCommand
{
    /**
     * @var RestCountriesClientService $restClient
     */
    public $restClient;

    /**
     * AbstractCommand constructor.
     */
    public function __construct()
    {
        $this->restClient = new RestCountriesClientService();
    }

    /**
     * Universal caller to controller
     *
     * @param string $controller
     * @param array $argv
     * @return mixed
     */
    public function callToController(string $controller, array $argv) {
        $controller = new $controller();
        return $controller->process($argv);
    }
}
