##### Command files
In those files you can define console commands what can use through index.php. You can run it in next case: `php index.php <AliasOfYourCommand>`. 

Also all additional arguments you will receive in $argv array to run method. Please see [InterfaceCommand.php](InterfaceCommand.php) where defined all required methods. 

Also in [AbstractCommand.php](AbstractCommand.php) implemented shared methods. Please use universal call controller callToController for convenient test your command. 