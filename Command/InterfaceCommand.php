<?php
namespace Command;

/**
 * Interface InterfaceCommand
 * @package Command
 */
interface InterfaceCommand
{
    /**
     * @param array $argv
     * @return string
     */
    public function run(array $argv):string;
}
