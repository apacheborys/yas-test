<?php
namespace Command;

use Controller\CompareCountryLangController;

/**
 * Class CompareCountriesCommand
 * Realize for compare languages in two countries
 *
 * @package Command
 */
class CompareCountriesCommand extends AbstractCommand
{
    /**
     * @param array $argv
     * @return string
     */
    public function run(array $argv):string
    {
        $result = $this->callToController(CompareCountryLangController::class, $argv);

        if (is_array($result) && isset($result['error']) && is_string($result['error'])) {
            return $result['error'];
        }

        if (!$result) {
            $output = $argv[1] . ' and ' . $argv[2] . ' do not speak the same language' . PHP_EOL;
        } else {
            $output = $argv[1] . ' and ' . $argv[2] . ' are speak the same language' . PHP_EOL;
        }

        return $output;
    }
}
