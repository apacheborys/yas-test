<?php
namespace Command;

use Controller\HelpByCountryController;

/**
 * Class HelpByCountryCommand
 * Helper for language country and finder countries in which people speaks in similar language
 *
 * @package Command
 */
class HelpByCountryCommand extends AbstractCommand
{
    /**
     * @param array $argv
     * @return string
     */
    public function run(array $argv):string
    {
        $result = $this->callToController(HelpByCountryController::class, $argv);

        if (is_array($result) && isset($result['error']) && is_string($result['error'])) {
            return $result['error'];
        }

        $nameCountries = [];
        foreach ($result['find'] as $country) {
            $nameCountries[] = $country->getName();
        }

        $output = 'Country language code: ' . implode(', ', $result['origin']->getLanguagesAsArray('iso639_1')) . PHP_EOL;
        $output .= $result['origin']->getName() . ' speaks same language with these countries: ';
        $output .= implode(', ', $nameCountries) . ' ...' . PHP_EOL;

        return $output;
    }
}
