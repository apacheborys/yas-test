<?php
namespace Service;

use Entity\CountryEntity;
use Mapper\EntityMapper;
use Validator\EntityValidator;

/**
 * Class RestCountriesClientService
 * REST client for restcountries.eu service @url https://restcountries.eu/#api-endpoints-all
 *
 * @package Service
 */
class RestCountriesClientService extends AbstractService
{
    const BASE_URL = 'https://restcountries.eu/rest/v2/';

    /**
     * @var HttpClientService $httpClient
     */
    public $httpClient;

    public function __construct()
    {
        $this->httpClient = new HttpClientService();
    }

    /**
     * @return CountryEntity[]|bool
     * @throws \ReflectionException
     */
    public function getAllCountries()
    {
        $response = $this->makeRequest('all');

        if ($this->httpClient->getResponseCode() !== 200) {
            echo 'Unexpected response code ' . $this->httpClient->getResponseCode();
            return false;
        }

        $parsedResponse = new JsonDataService($response);
        if ($parsedResponse->lastError) {
            echo 'Json data malformed. Error: ' . $parsedResponse->lastError;
            return false;
        }

        $validator = new EntityValidator();
        $validatorResult = $validator->validate($parsedResponse->resultArray);
        if (is_string($validatorResult)) {
            echo 'Validation are not successful. Reason: ' . $validatorResult;
            return false;
        }

        $mapper = new EntityMapper();
        $resultEntityArray = $mapper->map($parsedResponse->resultArray);

        return $resultEntityArray;
    }

    /**
     * Make HTTP request
     *
     * @param string $uri
     * @return string
     */
    public function makeRequest(string $uri):string
    {
        $this->httpClient->setOptions(
            [
                CURLOPT_URL             => self::BASE_URL . $uri,
                CURLOPT_RETURNTRANSFER  => true,
            ]
        );

        return $this->httpClient->exec();
    }
}
