<?php
namespace Service;

/**
 * Class HttpClientService
 * Simple http client based on curl
 *
 * @package Service
 */
class HttpClientService extends AbstractService
{
    /**
     * Instance of curl client
     *
     * @var resource $curlChannel
     */
    private $curlChannel;

    /**
     * Response body of last call
     *
     * @var string $response
     */
    public $response;

    /**
     * Response code of last call
     *
     * @var int $responseCode
     */
    public $responseCode;

    /**
     * HttpClientService constructor.
     */
    public function __construct()
    {
        $this->resetConnection();
    }

    /**
     * Reset connection for repeat-ready use
     */
    public function resetConnection()
    {
        if ($this->curlChannel) {
            unset($this->curlChannel);
        }

        $this->curlChannel = curl_init();
    }

    /**
     * Setter for option curl
     *
     * @param int $optionId
     * @param string $value
     * @return bool
     */
    public function setOption(int $optionId, string $value):bool
    {
        return curl_setopt($this->curlChannel, $optionId, $value);
    }

    /**
     * Setter for array of options
     *
     * @param array $options
     * @return bool
     */
    public function setOptions(array $options)
    {
        return curl_setopt_array($this->curlChannel, $options);
    }

    /**
     * Executing http request
     *
     * @return string
     */
    public function exec():string
    {
        $this->response = curl_exec($this->curlChannel);
        $this->responseCode = curl_getinfo($this->curlChannel, CURLINFO_HTTP_CODE);
        curl_close($this->curlChannel);

        return $this->response;
    }

    /**
     * Get response code of last call
     *
     * @return int
     */
    public function getResponseCode(): int
    {
        return $this->responseCode;
    }
}
