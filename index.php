<?php

include_once ('autoload.php');

if (!isset($argv[1])) {
    echo 'Please provide in first argument as country name';
    die();
}

if (isset($argv[2])) {
    $compareCountries = new \Command\CompareCountriesCommand();
    echo $compareCountries->run($argv);
    die();
}

$helpByCountry = new \Command\HelpByCountryCommand();
echo $helpByCountry->run($argv);
die();