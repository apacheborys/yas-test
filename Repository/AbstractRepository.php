<?php
namespace Repository;

use Entity\ValidateItemEntity;
use Validator\EntityValidator;

/**
 * Class AbstractRepository
 * @package Repository
 */
abstract class AbstractRepository implements InterfaceRepository
{
    /**
     * @var string $entity
     */
    public $entity;

    /**
     * @var array $data
     */
    public $data;

    /**
     * @var ValidateItemEntity[] $validationMap
     */
    public $validationMap;

    /**
     * AbstractRepository constructor.
     * @param string $entity
     */
    public function __construct(string $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @param array $entities
     * @throws \ReflectionException
     */
    public function loadData(array $entities)
    {
        $this->data = $entities;

        $validator = new EntityValidator();
        $this->validationMap = $validator->makeValidationMap($this->entity);
    }
}