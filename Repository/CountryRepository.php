<?php
namespace Repository;

use Entity\CountryEntity;

/**
 * Class CountryRepository
 * @package Repository
 */
class CountryRepository extends AbstractRepository
{
    /**
     * CountryRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(CountryEntity::class);
    }

    /**
     * Find entity with some specific value of specific key. You can set several keys with values. Array $options must
     * be associative array where key of array - property name of entity and value - value what we want to search
     *
     * @param array $options
     * @param int $limit
     * @return CountryEntity[]
     */
    public function findBy(array $options, int $limit = -1)
    {
        $result = [];

        /**
         * @var CountryEntity $country
         */
        foreach ($this->data as $indexNode => $country) {
            $accepted = true;
            foreach ($options as $findKey => $findValue) {
                if ($this->getValueByKey($country, $findKey) !== $findValue) {
                    $accepted = false;
                }
            }

            if ($accepted) {
                $result[] = $country;

                if ($limit > -1 && count($result) == $limit) {
                    return $result;
                }
            }
        }

        return $result;
    }

    /**
     * Find once result in same case as here @see CountryRepository::findBy()
     *
     * @param array $options
     * @return bool|CountryEntity
     */
    public function findByOne(array $options)
    {
        $result = $this->findBy($options, 1);

        if (isset($result[0])) {
            return $result[0];
        }

        return false;
    }

    /**
     * Find all countries with languages what specify in simple array where you can set iso2, iso3 or language name
     *
     * @param array $languages
     * @return array
     */
    public function findSimilarLanguage(array $languages)
    {
        $result = [];

        /**
         * @var CountryEntity $country
         */
        foreach ($this->data as $indexNode => $country) {
            $accepted = false;

            $countryLangs = $country->getLanguages();

            foreach ($languages as $language) {
                foreach ($countryLangs as $countryLang) {
                    if (
                        $countryLang->getIso639_1() === $language ||
                        $countryLang->getIso639_2() === $language ||
                        $countryLang->getName() === $language
                    ) {
                        $accepted = true;
                        break 2;
                    }
                }
            }

            if ($accepted) {
                $result[] = $country;
            }
        }

        return $result;
    }

    /**
     * Get value of specific entity key
     *
     * @param CountryEntity $country
     * @param $key
     * @return bool
     */
    private function getValueByKey(CountryEntity $country, $key)
    {
        if (!$this->validateRequestKey($key)) {
            return false;
        }

        eval('$result = $country->get' . ucfirst($key) . '();');

        return $result;
    }

    /**
     * Ensuring for existent request key
     *
     * @param string $key
     * @return bool
     */
    private function validateRequestKey(string $key)
    {
        if (!isset($this->validationMap[$key])) {
            return false;
        }

        return true;
    }
}