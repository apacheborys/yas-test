<?php
namespace Repository;

/**
 * Interface InterfaceRepository
 * @package Repository
 */
interface InterfaceRepository
{
    /**
     * @param array $options
     * @return mixed
     */
    public function findBy(array $options);
}